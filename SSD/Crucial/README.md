Crucial Solid State Drives
==========================

This is a list of all tested Crucial solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Crucial   | CT120M500SSD3      | 120 GB | 2       | 933   | 8     | 2.54   |
| Crucial   | M4-CT256M4SSD2     | 256 GB | 8       | 925   | 256   | 2.47   |
| Crucial   | M4-CT128M4SSD1     | 128 GB | 4       | 902   | 0     | 2.47   |
| Crucial   | CT250MX200SSD3     | 250 GB | 1       | 816   | 0     | 2.24   |
| Crucial   | M4-CT128M4SSD2     | 128 GB | 20      | 828   | 51    | 2.22   |
| Crucial   | M4-CT064M4SSD2     | 64 GB  | 9       | 702   | 0     | 1.93   |
| Crucial   | CT1050MX300SSD1    | 1 TB   | 2       | 476   | 0     | 1.31   |
| Crucial   | CT256MX100SSD1     | 256 GB | 12      | 383   | 0     | 1.05   |
| Crucial   | C300-CTFDDAC128MAG | 128 GB | 3       | 330   | 0     | 0.91   |
| Crucial   | CT275MX300SSD4     | 275 GB | 1       | 316   | 0     | 0.87   |
| Crucial   | C300-CTFDDAC064MAG | 64 GB  | 1       | 307   | 0     | 0.84   |
| Crucial   | CT240M500SSD1      | 240 GB | 12      | 475   | 184   | 0.72   |
| Crucial   | CT500MX200SSD1     | 500 GB | 6       | 260   | 0     | 0.71   |
| Crucial   | CT512MX100SSD1     | 512 GB | 6       | 252   | 0     | 0.69   |
| Crucial   | M4-CT128M4SSD3     | 128 GB | 1       | 238   | 0     | 0.65   |
| Crucial   | CT240M500SSD3      | 240 GB | 3       | 342   | 11    | 0.65   |
| Crucial   | CT120M500SSD1      | 120 GB | 9       | 436   | 8     | 0.63   |
| Crucial   | CT500MX200SSD4     | 500 GB | 2       | 222   | 0     | 0.61   |
| Crucial   | M4-CT512M4SSD2     | 512 GB | 1       | 184   | 0     | 0.51   |
| Crucial   | CT2000MX500SSD1    | 2 TB   | 1       | 183   | 0     | 0.50   |
| Crucial   | CT960M500SSD1      | 960 GB | 4       | 445   | 517   | 0.49   |
| Crucial   | M4-CT512M4SSD1     | 512 GB | 1       | 161   | 0     | 0.44   |
| Crucial   | CT250MX200SSD1     | 250 GB | 4       | 159   | 0     | 0.44   |
| Crucial   | CT750MX300SSD1     | 752 GB | 4       | 137   | 0     | 0.38   |
| Crucial   | CT256M550SSD1      | 256 GB | 5       | 378   | 7     | 0.34   |
| Crucial   | CT1024MX200SSD1    | 1 TB   | 1       | 116   | 0     | 0.32   |
| Crucial   | CT240BX300SSD1     | 240 GB | 1       | 113   | 0     | 0.31   |
| Crucial   | CT512M550SSD3      | 512 GB | 2       | 109   | 193   | 0.30   |
| Crucial   | CT128M550SSD3      | 128 GB | 3       | 212   | 11    | 0.20   |
| Crucial   | CT525MX300SSD1     | 528 GB | 5       | 77    | 4     | 0.19   |
| Crucial   | CT128M550SSD1      | 128 GB | 3       | 88    | 30    | 0.17   |
| Crucial   | CT250BX100SSD1     | 250 GB | 4       | 54    | 0     | 0.15   |
| Crucial   | CT275MX300SSD1     | 275 GB | 10      | 45    | 1     | 0.13   |
| Crucial   | CT1000MX500SSD1    | 1 TB   | 2       | 45    | 0     | 0.12   |
| Crucial   | CT120BX300SSD1     | 120 GB | 2       | 37    | 0     | 0.10   |
| Crucial   | CT120BX500SSD1     | 120 GB | 10      | 36    | 0     | 0.10   |
| Crucial   | CT500MX500SSD4     | 500 GB | 2       | 28    | 0     | 0.08   |
| Crucial   | CT480BX500SSD1     | 480 GB | 2       | 25    | 0     | 0.07   |
| Crucial   | CT250MX500SSD1     | 250 GB | 3       | 23    | 0     | 0.06   |
| Crucial   | CT2050MX300SSD1    | 2 TB   | 2       | 29    | 1     | 0.04   |
| Crucial   | CT240BX200SSD1     | 240 GB | 3       | 14    | 0     | 0.04   |
| Crucial   | CT500MX500SSD1     | 500 GB | 6       | 12    | 0     | 0.03   |
| Crucial   | V4-CT128V4SSD2     | 128 GB | 1       | 11    | 0     | 0.03   |
| Crucial   | CT480BX200SSD1     | 480 GB | 2       | 11    | 0     | 0.03   |
| Crucial   | CT240BX500SSD1     | 240 GB | 4       | 10    | 0     | 0.03   |
| Crucial   | CT480M500SSD1      | 480 GB | 1       | 166   | 16    | 0.03   |
| Crucial   | CT525MX300SSD4     | 528 GB | 1       | 4     | 0     | 0.01   |
| Crucial   | CT512M550SSD1      | 512 GB | 1       | 63    | 16    | 0.01   |
| Crucial   | C300-CTFDDAC256MAG | 256 GB | 1       | 847   | 1008  | 0.00   |
| Crucial   | CT128MX100SSD1     | 128 GB | 1       | 3     | 16    | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Crucial   | RealSSD m4/C400        | 5      | 34      | 822   | 90    | 2.21   |
| Crucial   | RealSSD m4/C400/P400   | 2      | 10      | 651   | 0     | 1.78   |
| Crucial   | MX100/MX200/M5x0/M6... | 7      | 27      | 351   | 82    | 0.72   |
| Crucial   | MX100/M500/M510/M55... | 1      | 6       | 252   | 0     | 0.69   |
| Crucial   | RealSSD C300/M500      | 6      | 29      | 418   | 80    | 0.69   |
| Crucial   | MX1/2/300, M5/600, ... | 7      | 27      | 167   | 17    | 0.41   |
| Crucial   | BX/MX1/2/3/500, M5/... | 17     | 51      | 93    | 1     | 0.25   |
| Crucial   | SiliconMotion based... | 3      | 9       | 31    | 0     | 0.09   |
| Crucial   | Unknown                | 1      | 1       | 11    | 0     | 0.03   |
| Crucial   | RealSSD C300/P300      | 1      | 1       | 847   | 1008  | 0.00   |
