China Solid State Drives
========================

This is a list of all tested China solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| China     | SATA SSD           | 20 GB  | 7       | 383   | 1     | 0.93   |
| China     | SSD                | 64 GB  | 5       | 210   | 1     | 0.47   |
| China     | SATA SSD           | 128 GB | 4       | 149   | 0     | 0.41   |
| China     | SATA SSD           | 64 GB  | 10      | 76    | 0     | 0.21   |
| China     | 64GB SSD           | 64 GB  | 10      | 68    | 0     | 0.19   |
| China     | 128GB SSD          | 128 GB | 5       | 66    | 0     | 0.18   |
| China     | 120GB SSD          | 120 GB | 25      | 62    | 0     | 0.17   |
| China     | SSD 120G           | 120 GB | 2       | 54    | 0     | 0.15   |
| China     | 80GB SSD           | 80 GB  | 2       | 47    | 0     | 0.13   |
| China     | SSD128G            | 128 GB | 1       | 42    | 0     | 0.12   |
| China     | 240GB SSD          | 240 GB | 1       | 41    | 0     | 0.11   |
| China     | SATA SSD           | 120 GB | 11      | 39    | 0     | 0.11   |
| China     | SATA SSD           | 240 GB | 5       | 34    | 0     | 0.09   |
| China     | SSD                | 120 GB | 4       | 32    | 0     | 0.09   |
| China     | 60GB SSD           | 64 GB  | 1       | 17    | 0     | 0.05   |
| China     | RTMMB256VBV4KFY    | 256 GB | 1       | 17    | 0     | 0.05   |
| China     | SSD                | 240 GB | 6       | 36    | 1     | 0.05   |
| China     | SSD                | 128 GB | 4       | 16    | 0     | 0.04   |
| China     | SSD                | 480 GB | 2       | 11    | 0     | 0.03   |
| China     | SSD60G             | 64 GB  | 2       | 1     | 0     | 0.01   |
| China     | SSD 256G           | 256 GB | 1       | 1     | 0     | 0.00   |
| China     | SATA SSD           | 480 GB | 1       | 1     | 0     | 0.00   |
| China     | T60                | 64 GB  | 2       | 0     | 0     | 0.00   |
| China     | T120               | 120 GB | 1       | 0     | 0     | 0.00   |
