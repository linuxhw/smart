KingDian Solid State Drives
===========================

This is a list of all tested KingDian solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| KingDian  | S400 XT            | 240 GB | 1       | 177   | 0     | 0.49   |
| KingDian  | S400               | 120 GB | 6       | 120   | 0     | 0.33   |
| KingDian  | S400               | 480 GB | 1       | 73    | 0     | 0.20   |
| KingDian  | S200               | 120 GB | 2       | 49    | 0     | 0.13   |
| KingDian  | S280-240GB         | 240 GB | 7       | 43    | 0     | 0.12   |
| KingDian  | S180               | 64 GB  | 10      | 38    | 114   | 0.09   |
| KingDian  | S280               | 240 GB | 7       | 20    | 0     | 0.06   |
| KingDian  | S280               | 120 GB | 4       | 20    | 0     | 0.06   |
| KingDian  | S280-120GB         | 120 GB | 2       | 9     | 0     | 0.03   |
| KingDian  | S280               | 480 GB | 1       | 9     | 0     | 0.02   |
| KingDian  | N400 60G           | 64 GB  | 1       | 8     | 0     | 0.02   |
| KingDian  | S200               | 64 GB  | 4       | 6     | 0     | 0.02   |
| KingDian  | S180               | 120 GB | 1       | 0     | 0     | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| KingDian  | SiliconMotion based... | 2      | 7       | 113   | 0     | 0.31   |
| KingDian  | Unknown                | 11     | 40      | 31    | 29    | 0.08   |
