Kingston Solid State Drives
===========================

This is a list of all tested Kingston solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Kingston  | SH100S3120G        | 120 GB | 2       | 1711  | 0     | 4.69   |
| Kingston  | SKC300S37A480G     | 480 GB | 1       | 1231  | 0     | 3.37   |
| Kingston  | SVP100S296G        | 96 GB  | 1       | 918   | 0     | 2.52   |
| Kingston  | SE50S3480G         | 480 GB | 2       | 859   | 0     | 2.35   |
| Kingston  | SVP200S37A480G     | 480 GB | 1       | 765   | 0     | 2.10   |
| Kingston  | SVP100S2128G       | 128 GB | 1       | 698   | 0     | 1.91   |
| Kingston  | SVP200S390G        | 90 GB  | 1       | 686   | 0     | 1.88   |
| Kingston  | SNVP325S2128GB     | 128 GB | 1       | 674   | 0     | 1.85   |
| Kingston  | RBU-SNS8152S3128GF | 128 GB | 1       | 665   | 0     | 1.82   |
| Kingston  | SV200S364G         | 64 GB  | 2       | 645   | 0     | 1.77   |
| Kingston  | SVP200S37A240G     | 240 GB | 1       | 623   | 0     | 1.71   |
| Kingston  | SVP200S360G        | 64 GB  | 4       | 572   | 0     | 1.57   |
| Kingston  | SMS100S264G        | 64 GB  | 2       | 547   | 0     | 1.50   |
| Kingston  | SH100S3240G        | 240 GB | 2       | 1170  | 2     | 1.42   |
| Kingston  | SVP200S37A120G     | 120 GB | 7       | 502   | 154   | 1.20   |
| Kingston  | SV200S3128G        | 128 GB | 6       | 432   | 0     | 1.19   |
| Kingston  | SKC380S360G        | 64 GB  | 1       | 420   | 0     | 1.15   |
| Kingston  | SMS200S3240G       | 240 GB | 1       | 419   | 0     | 1.15   |
| Kingston  | SNV425S264GB       | 64 GB  | 1       | 386   | 0     | 1.06   |
| Kingston  | SM2280S3G2240G     | 240 GB | 2       | 380   | 0     | 1.04   |
| Kingston  | SH103S3120G        | 120 GB | 36      | 358   | 0     | 0.98   |
| Kingston  | SVP200S37A60G      | 64 GB  | 6       | 345   | 0     | 0.95   |
| Kingston  | SVP200S3240G       | 240 GB | 2       | 307   | 0     | 0.84   |
| Kingston  | SNVP325S2256GB     | 256 GB | 1       | 293   | 0     | 0.80   |
| Kingston  | SH103S3240G        | 240 GB | 10      | 467   | 213   | 0.78   |
| Kingston  | SKC300S37A120G     | 120 GB | 12      | 280   | 0     | 0.77   |
| Kingston  | SMS100S232G        | 32 GB  | 1       | 279   | 0     | 0.76   |
| Kingston  | SMS200S360G        | 64 GB  | 8       | 282   | 1     | 0.75   |
| Kingston  | SKC400S37128G      | 128 GB | 2       | 264   | 0     | 0.72   |
| Kingston  | SKC300S37A60G      | 64 GB  | 12      | 373   | 6     | 0.70   |
| Kingston  | SV300S37A60G       | 64 GB  | 94      | 250   | 1     | 0.68   |
| Kingston  | SM2280S3120G       | 120 GB | 3       | 230   | 0     | 0.63   |
| Kingston  | SV100S264G         | 64 GB  | 6       | 391   | 4     | 0.63   |
| Kingston  | SVP200S37A90G      | 90 GB  | 2       | 223   | 0     | 0.61   |
| Kingston  | SUV300S37A480G     | 480 GB | 1       | 222   | 0     | 0.61   |
| Kingston  | SV100S232G         | 32 GB  | 3       | 219   | 0     | 0.60   |
| Kingston  | SV200S3256G        | 256 GB | 3       | 676   | 3     | 0.59   |
| Kingston  | SV300S37A120G      | 120 GB | 270     | 252   | 22    | 0.58   |
| Kingston  | RBU-SNS8152S312... | 128 GB | 2       | 190   | 0     | 0.52   |
| Kingston  | SS100S216G         | 16 GB  | 1       | 190   | 0     | 0.52   |
| Kingston  | SV100S2128G        | 128 GB | 2       | 359   | 2     | 0.52   |
| Kingston  | SUV400S37480G      | 480 GB | 1       | 164   | 0     | 0.45   |
| Kingston  | SM2280S3G2120G     | 120 GB | 2       | 162   | 0     | 0.44   |
| Kingston  | SMS200S3120G       | 120 GB | 7       | 167   | 110   | 0.44   |
| Kingston  | SHSS37A120G        | 120 GB | 6       | 148   | 0     | 0.41   |
| Kingston  | SV300S37A240G      | 240 GB | 52      | 135   | 1     | 0.37   |
| Kingston  | SMS200S330G        | 32 GB  | 1       | 132   | 0     | 0.36   |
| Kingston  | SUV400S37240G      | 240 GB | 39      | 131   | 27    | 0.34   |
| Kingston  | RBU-SNS8152S312... | 128 GB | 1       | 120   | 0     | 0.33   |
| Kingston  | SHSS37A480G        | 480 GB | 5       | 119   | 0     | 0.33   |
| Kingston  | SUV300S37A240G     | 240 GB | 7       | 118   | 0     | 0.32   |
| Kingston  | SVP200S3120G       | 120 GB | 4       | 370   | 499   | 0.32   |
| Kingston  | RBUSNS8280S3128GH2 | 128 GB | 2       | 112   | 0     | 0.31   |
| Kingston  | SUV300S37A120G     | 120 GB | 18      | 112   | 0     | 0.31   |
| Kingston  | SKC400S37512G      | 512 GB | 1       | 99    | 0     | 0.27   |
| Kingston  | SUV400S37120G      | 120 GB | 41      | 105   | 9     | 0.25   |
| Kingston  | RBU-SNS8151S396GG  | 96 GB  | 1       | 88    | 0     | 0.24   |
| Kingston  | SKC380S3120G       | 120 GB | 1       | 86    | 0     | 0.24   |
| Kingston  | SV300S37A480G      | 480 GB | 7       | 91    | 144   | 0.23   |
| Kingston  | RBU-SNS8152S325... | 256 GB | 1       | 80    | 0     | 0.22   |
| Kingston  | SHSS37A240G        | 240 GB | 12      | 77    | 0     | 0.21   |
| Kingston  | SS200S330G         | 32 GB  | 1       | 70    | 0     | 0.19   |
| Kingston  | SHFS37A240G        | 240 GB | 12      | 96    | 424   | 0.19   |
| Kingston  | RBU-SNS8100S3256GD | 256 GB | 1       | 66    | 0     | 0.18   |
| Kingston  | SKC400S37256G      | 256 GB | 1       | 64    | 0     | 0.18   |
| Kingston  | SHPM2280P2H-480G   | 480 GB | 1       | 180   | 2     | 0.16   |
| Kingston  | SUV500480G         | 480 GB | 1       | 57    | 0     | 0.16   |
| Kingston  | SHFS37A120G        | 120 GB | 52      | 131   | 412   | 0.15   |
| Kingston  | SA400S37240G       | 240 GB | 26      | 67    | 5     | 0.14   |
| Kingston  | RBU-SC152S37128GG2 | 128 GB | 1       | 45    | 0     | 0.12   |
| Kingston  | SA400S37120G       | 120 GB | 68      | 40    | 1     | 0.11   |
| Kingston  | SH103S3480G        | 480 GB | 1       | 38    | 0     | 0.11   |
| Kingston  | RBUSNS8180S3128GI1 | 128 GB | 1       | 38    | 0     | 0.11   |
| Kingston  | RBUSNS8180S3128GJ  | 128 GB | 1       | 29    | 0     | 0.08   |
| Kingston  | SA400S37480G       | 480 GB | 9       | 30    | 10    | 0.07   |
| Kingston  | SKC300S37A180G     | 180 GB | 2       | 166   | 1025  | 0.06   |
| Kingston  | SUV500120G         | 120 GB | 4       | 18    | 0     | 0.05   |
| Kingston  | RBU-SNS8100S312... | 128 GB | 2       | 8     | 0     | 0.02   |
| Kingston  | SKC300S37A240G     | 240 GB | 2       | 1     | 0     | 0.01   |
| Kingston  | RBUSNS8180DS3256GJ | 256 GB | 1       | 1     | 0     | 0.00   |
| Kingston  | SA400S37           | 240 GB | 1       | 0     | 0     | 0.00   |
| Kingston  | SH103S3240G-NV     | 240 GB | 1       | 377   | 1018  | 0.00   |
| Kingston  | SVP200S37A256G     | 256 GB | 1       | 311   | 1017  | 0.00   |
| Kingston  | SHPM2280P2H-240G   | 240 GB | 1       | 80    | 478   | 0.00   |
| Kingston  | SUV500240G         | 240 GB | 1       | 168   | 1151  | 0.00   |
| Kingston  | SMS151S324G        | 24 GB  | 1       | 95    | 1022  | 0.00   |
| Kingston  | SUV500MS120G       | 120 GB | 2       | 0     | 0     | 0.00   |
| Kingston  | SNS4151S316GD      | 16 GB  | 1       | 51    | 1022  | 0.00   |
| Kingston  | SNS4151S316G       | 16 GB  | 1       | 39    | 1022  | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Kingston  | JMicron based SSDs     | 11     | 25      | 485   | 2     | 1.04   |
| Kingston  | SandForce Driven SSDs  | 32     | 616     | 257   | 69    | 0.61   |
| Kingston  | Unknown                | 34     | 126     | 135   | 57    | 0.33   |
| Kingston  | Phison Driven SSDs     | 12     | 156     | 70    | 2     | 0.18   |
