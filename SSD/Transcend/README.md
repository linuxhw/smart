Transcend Solid State Drives
============================

This is a list of all tested Transcend solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Transcend | TS128GSSD320       | 128 GB | 1       | 493   | 0     | 1.35   |
| Transcend | TS128GSSD340       | 128 GB | 4       | 283   | 0     | 0.78   |
| Transcend | TS128GSSD720       | 128 GB | 2       | 187   | 0     | 0.51   |
| Transcend | TS128GMSA740       | 128 GB | 1       | 180   | 0     | 0.50   |
| Transcend | TS256GMTS400       | 256 GB | 3       | 132   | 0     | 0.36   |
| Transcend | TS128GMTS400S      | 128 GB | 1       | 113   | 0     | 0.31   |
| Transcend | TS120GSSD25D-M     | 128 GB | 1       | 309   | 2     | 0.28   |
| Transcend | TS256GSSD320       | 256 GB | 2       | 84    | 0     | 0.23   |
| Transcend | TS256GSSD340       | 256 GB | 2       | 76    | 0     | 0.21   |
| Transcend | TS256GMTS800       | 256 GB | 2       | 69    | 0     | 0.19   |
| Transcend | TS64GMSA370        | 64 GB  | 2       | 65    | 0     | 0.18   |
| Transcend | TS128GSSD370       | 128 GB | 6       | 63    | 0     | 0.17   |
| Transcend | TS64GSSD340        | 64 GB  | 1       | 54    | 0     | 0.15   |
| Transcend | TS256GSSD230S      | 256 GB | 1       | 48    | 0     | 0.13   |
| Transcend | TS256GMTS430S      | 256 GB | 2       | 36    | 0     | 0.10   |
| Transcend | TS128GSSD370S      | 128 GB | 14      | 29    | 0     | 0.08   |
| Transcend | TS128GMTS400       | 128 GB | 1       | 29    | 0     | 0.08   |
| Transcend | TS64GSSD720        | 64 GB  | 1       | 26    | 0     | 0.07   |
| Transcend | TS64GSSD320        | 64 GB  | 1       | 26    | 0     | 0.07   |
| Transcend | TS240GSSD220S      | 240 GB | 4       | 25    | 0     | 0.07   |
| Transcend | TS256GSSD370S      | 256 GB | 2       | 23    | 0     | 0.06   |
| Transcend | TS120GSSD220S      | 120 GB | 5       | 21    | 0     | 0.06   |
| Transcend | TS128GSSD230S      | 128 GB | 6       | 20    | 0     | 0.06   |
| Transcend | TS128GMTS800       | 128 GB | 3       | 19    | 0     | 0.05   |
| Transcend | TS32GSSD370S       | 32 GB  | 4       | 15    | 0     | 0.04   |
| Transcend | TS64GSSD25S-M      | 64 GB  | 2       | 15    | 0     | 0.04   |
| Transcend | TS128GSSD360S      | 128 GB | 6       | 13    | 0     | 0.04   |
| Transcend | TS128GMTS600       | 128 GB | 1       | 13    | 0     | 0.04   |
| Transcend | TS512GMSA370       | 512 GB | 1       | 12    | 0     | 0.03   |
| Transcend | TS240GMTS820S      | 240 GB | 1       | 6     | 0     | 0.02   |
| Transcend | TS64GSSD370        | 64 GB  | 2       | 4     | 0     | 0.01   |
| Transcend | TS256GSSD360S      | 256 GB | 2       | 2     | 0     | 0.01   |
| Transcend | TS64GMTS800        | 64 GB  | 1       | 2     | 0     | 0.01   |
| Transcend | TS32GMSA370        | 32 GB  | 1       | 1     | 0     | 0.01   |
| Transcend | TS128GMSA370       | 128 GB | 1       | 1     | 0     | 0.00   |
| Transcend | TS120GMTS820S      | 120 GB | 1       | 0     | 0     | 0.00   |
| Transcend | TS64GSSD340K       | 64 GB  | 1       | 74    | 1008  | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Transcend | SandForce Driven SSDs  | 5      | 7       | 155   | 0     | 0.43   |
| Transcend | JMicron based SSDs     | 6      | 11      | 148   | 92    | 0.39   |
| Transcend | Indilinx Barefoot b... | 1      | 1       | 309   | 2     | 0.28   |
| Transcend | SiliconMotion based... | 19     | 60      | 36    | 0     | 0.10   |
| Transcend | Unknown                | 6      | 13      | 13    | 0     | 0.04   |
