Gigabyte Solid State Drives
===========================

This is a list of all tested Gigabyte solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Gigabyte  | GP-GSTFS31120GNTD  | 120 GB | 1       | 3     | 0     | 0.01   |
| Gigabyte  | GP-GSTFS31240GNTD  | 240 GB | 2       | 1     | 0     | 0.01   |
