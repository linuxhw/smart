SanDisk Solid State Drives
==========================

This is a list of all tested SanDisk solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| SanDisk   | SD7UB2Q512G1122    | 512 GB | 1       | 1017  | 0     | 2.79   |
| SanDisk   | SD5SE2128G1002E    | 128 GB | 2       | 729   | 0     | 2.00   |
| SanDisk   | SD6SB1M256G1002    | 256 GB | 1       | 548   | 0     | 1.50   |
| SanDisk   | SDSSDH240GG25      | 240 GB | 1       | 473   | 0     | 1.30   |
| SanDisk   | SD8SN8U256G1122    | 256 GB | 1       | 434   | 0     | 1.19   |
| SanDisk   | SD6SB2M128G1022I   | 128 GB | 1       | 412   | 0     | 1.13   |
| SanDisk   | SDSSDH2256G        | 256 GB | 1       | 403   | 0     | 1.10   |
| SanDisk   | SDSSDXP240G        | 240 GB | 2       | 398   | 0     | 1.09   |
| SanDisk   | SDSSDH2128G        | 128 GB | 1       | 356   | 0     | 0.98   |
| SanDisk   | SDSSDHII480G       | 480 GB | 4       | 344   | 0     | 0.95   |
| SanDisk   | SDSSDHP064G        | 64 GB  | 8       | 316   | 0     | 0.87   |
| SanDisk   | Ultra II           | 960 GB | 3       | 502   | 1     | 0.86   |
| SanDisk   | SDSSDX120GG25      | 120 GB | 4       | 301   | 0     | 0.83   |
| SanDisk   | SSD U100           | 128 GB | 6       | 390   | 33    | 0.81   |
| SanDisk   | X300 2.5 7MM       | 256 GB | 1       | 297   | 0     | 0.81   |
| SanDisk   | SD6SB1M128G1001    | 128 GB | 1       | 271   | 0     | 0.74   |
| SanDisk   | SDSSDHP128G        | 128 GB | 17      | 285   | 1     | 0.74   |
| SanDisk   | SD8SN8U512G1002    | 512 GB | 3       | 268   | 0     | 0.74   |
| SanDisk   | iSSD P4            | 8 GB   | 3       | 350   | 1     | 0.73   |
| SanDisk   | SD6SB1M128G        | 128 GB | 1       | 256   | 0     | 0.70   |
| SanDisk   | SDSSDP064G         | 64 GB  | 5       | 294   | 205   | 0.66   |
| SanDisk   | SSD i100           | 16 GB  | 5       | 233   | 0     | 0.64   |
| SanDisk   | SDSSDH31000G       | 1 TB   | 1       | 231   | 0     | 0.64   |
| SanDisk   | Z400s M.2 2280     | 256 GB | 1       | 229   | 0     | 0.63   |
| SanDisk   | SD5SG2256G1052E    | 256 GB | 2       | 222   | 0     | 0.61   |
| SanDisk   | SSD U110           | 64 GB  | 1       | 222   | 0     | 0.61   |
| SanDisk   | X400 M.2 2280      | 256 GB | 2       | 208   | 0     | 0.57   |
| SanDisk   | SD8SN8U128G1002    | 128 GB | 1       | 206   | 0     | 0.57   |
| SanDisk   | X400 M.2 2280      | 128 GB | 1       | 202   | 0     | 0.56   |
| SanDisk   | SD7TB6S256G1001    | 256 GB | 1       | 200   | 0     | 0.55   |
| SanDisk   | SD5SF2128G1014E    | 128 GB | 2       | 200   | 0     | 0.55   |
| SanDisk   | SD6SF1M128G1022I   | 128 GB | 1       | 396   | 1     | 0.54   |
| SanDisk   | SDSSDHII120G       | 120 GB | 17      | 178   | 0     | 0.49   |
| SanDisk   | SD7SB3Q256G1002    | 256 GB | 1       | 355   | 1     | 0.49   |
| SanDisk   | SDSSDP128G         | 128 GB | 20      | 174   | 1     | 0.48   |
| SanDisk   | SDSSDP256G         | 256 GB | 2       | 169   | 0     | 0.46   |
| SanDisk   | SD7SB6S256G1122    | 256 GB | 1       | 163   | 0     | 0.45   |
| SanDisk   | SSD i100           | 24 GB  | 12      | 156   | 3     | 0.41   |
| SanDisk   | SDSSDX240GG25      | 240 GB | 1       | 1348  | 8     | 0.41   |
| SanDisk   | SDSSDHII240G       | 240 GB | 9       | 146   | 0     | 0.40   |
| SanDisk   | SDSSDHP256G        | 256 GB | 5       | 135   | 0     | 0.37   |
| SanDisk   | SDSSDXPS240G       | 240 GB | 4       | 136   | 259   | 0.35   |
| SanDisk   | SD6SB1M128G1002    | 128 GB | 1       | 110   | 0     | 0.30   |
| SanDisk   | Ultra II           | 480 GB | 2       | 109   | 0     | 0.30   |
| SanDisk   | SD8TN8U256G1001    | 256 GB | 1       | 109   | 0     | 0.30   |
| SanDisk   | SD7SB6S-128G-1006  | 128 GB | 1       | 108   | 0     | 0.30   |
| SanDisk   | SSD U100           | 16 GB  | 1       | 105   | 0     | 0.29   |
| SanDisk   | Ultra II           | 240 GB | 6       | 100   | 0     | 0.28   |
| SanDisk   | SD8SN8U512G1122    | 512 GB | 1       | 98    | 0     | 0.27   |
| SanDisk   | SD7SB3Q128G1002    | 128 GB | 1       | 387   | 3     | 0.27   |
| SanDisk   | SSD U100           | 256 GB | 1       | 94    | 0     | 0.26   |
| SanDisk   | SD7SN3Q128G1002    | 128 GB | 1       | 85    | 0     | 0.23   |
| SanDisk   | SSD i100           | 8 GB   | 2       | 77    | 0     | 0.21   |
| SanDisk   | SD8SB8U256G1122    | 256 GB | 2       | 72    | 0     | 0.20   |
| SanDisk   | SD5SE2256G1002E    | 256 GB | 1       | 68    | 0     | 0.19   |
| SanDisk   | PLUS               | 480 GB | 2       | 68    | 0     | 0.19   |
| SanDisk   | SD8TB8U512G1001    | 512 GB | 1       | 66    | 0     | 0.18   |
| SanDisk   | SSD U110           | 16 GB  | 9       | 63    | 0     | 0.17   |
| SanDisk   | SDSSDA240G         | 240 GB | 40      | 61    | 0     | 0.17   |
| SanDisk   | SSD U100           | 24 GB  | 15      | 70    | 19    | 0.17   |
| SanDisk   | SDSSDH2064G        | 64 GB  | 1       | 58    | 0     | 0.16   |
| SanDisk   | SSD PLUS 240 GB    | 240 GB | 5       | 53    | 0     | 0.15   |
| SanDisk   | SDSSDA960G         | 960 GB | 1       | 52    | 0     | 0.14   |
| SanDisk   | SD5SF2032G1010E    | 32 GB  | 1       | 45    | 0     | 0.13   |
| SanDisk   | SDSSDH3512G        | 512 GB | 1       | 38    | 0     | 0.10   |
| SanDisk   | SD6SB1M128G1022I   | 128 GB | 2       | 37    | 0     | 0.10   |
| SanDisk   | SSD PLUS 120 GB    | 120 GB | 7       | 37    | 0     | 0.10   |
| SanDisk   | SD6SB1M-032G-1006  | 32 GB  | 1       | 33    | 0     | 0.09   |
| SanDisk   | SDSSDXP120G        | 120 GB | 1       | 32    | 0     | 0.09   |
| SanDisk   | SDSSDA480G         | 480 GB | 3       | 31    | 0     | 0.09   |
| SanDisk   | SSD i100           | 32 GB  | 2       | 30    | 0     | 0.08   |
| SanDisk   | SSD PLUS           | 240 GB | 4       | 30    | 0     | 0.08   |
| SanDisk   | SDSSDA120G         | 120 GB | 27      | 33    | 4     | 0.08   |
| SanDisk   | SDSA5DK-016G-1006  | 16 GB  | 1       | 29    | 0     | 0.08   |
| SanDisk   | SD8SN8U-128G-1006  | 128 GB | 10      | 40    | 3     | 0.05   |
| SanDisk   | SSD U100           | 64 GB  | 5       | 18    | 0     | 0.05   |
| SanDisk   | SDSSDH3250G        | 250 GB | 1       | 18    | 0     | 0.05   |
| SanDisk   | SD8SN8U1T001122    | 1 TB   | 1       | 17    | 0     | 0.05   |
| SanDisk   | SD6SB1M-128G-1006  | 128 GB | 1       | 16    | 0     | 0.05   |
| SanDisk   | SD8SBAT128G1122    | 128 GB | 1       | 15    | 0     | 0.04   |
| SanDisk   | SSD PLUS           | 120 GB | 3       | 12    | 0     | 0.04   |
| SanDisk   | SSD P4             | 32 GB  | 9       | 92    | 134   | 0.03   |
| SanDisk   | SD7SN6S-128G-1006  | 128 GB | 1       | 8     | 0     | 0.02   |
| SanDisk   | SD8SNAT256G1002    | 256 GB | 2       | 7     | 0     | 0.02   |
| SanDisk   | SD8SBAT256G1122    | 256 GB | 3       | 6     | 0     | 0.02   |
| SanDisk   | SD8SNAT128G1002    | 128 GB | 2       | 5     | 0     | 0.02   |
| SanDisk   | SD8SB8U512G1122    | 512 GB | 1       | 4     | 0     | 0.01   |
| SanDisk   | SATAIII            | 16 GB  | 1       | 3     | 0     | 0.01   |
| SanDisk   | SSD i110           | 128 GB | 1       | 2     | 0     | 0.01   |
| SanDisk   | SDSA5GK-016G-1006  | 16 GB  | 1       | 276   | 106   | 0.01   |
| SanDisk   | SSD i110           | 16 GB  | 2       | 2     | 0     | 0.01   |
| SanDisk   | SSD i110           | 24 GB  | 1       | 2     | 0     | 0.01   |
| SanDisk   | SDSSDH120GG25      | 120 GB | 1       | 112   | 55    | 0.01   |
| SanDisk   | SD8SB8U128G1001    | 128 GB | 1       | 1     | 0     | 0.00   |
| SanDisk   | SD8SNAT256G1122    | 256 GB | 1       | 1     | 0     | 0.00   |
| SanDisk   | SSD P4             | 64 GB  | 1       | 14    | 14    | 0.00   |
| SanDisk   | SD8SNAT-128G-1006  | 128 GB | 1       | 0     | 0     | 0.00   |
| SanDisk   | SD6SB1M256G1022I   | 256 GB | 1       | 400   | 474   | 0.00   |
| SanDisk   | SD7UB3Q256G1001    | 256 GB | 1       | 71    | 100   | 0.00   |
| SanDisk   | SD8TN8U512G1001    | 512 GB | 1       | 0     | 0     | 0.00   |
| SanDisk   | SSD U100           | 32 GB  | 2       | 8     | 503   | 0.00   |
| SanDisk   | SDSSDXPS960G       | 960 GB | 1       | 13    | 39    | 0.00   |
| SanDisk   | SD8SNAT-256G-1006  | 256 GB | 1       | 0     | 0     | 0.00   |
| SanDisk   | SD8SBAT-032G-1006  | 32 GB  | 3       | 0     | 0     | 0.00   |
| SanDisk   | X600 M.2 2280 SATA | 128 GB | 1       | 0     | 0     | 0.00   |
| SanDisk   | SD8SN8U-256G-1006  | 256 GB | 3       | 175   | 1023  | 0.00   |
| SanDisk   | CF Card            | 16 GB  | 1       | 0     | 0     | 0.00   |
| SanDisk   | SDSSDX480GG25      | 480 GB | 1       | 76    | 1090  | 0.00   |
| SanDisk   | SD9SN8W-128G-1006  | 128 GB | 4       | 17    | 998   | 0.00   |
| SanDisk   | TE22D10400GE8001   | 400 GB | 1       | 34    | 2047  | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| SanDisk   | Marvell based SanDi... | 33     | 113     | 198   | 42    | 0.49   |
| SanDisk   | SanDisk based SSDs     | 20     | 95      | 167   | 28    | 0.41   |
| SanDisk   | Unknown                | 51     | 90      | 94    | 83    | 0.22   |
| SanDisk   | SandForce Driven SSDs  | 6      | 76      | 79    | 16    | 0.17   |
