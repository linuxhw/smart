Samsung Hard Drives
===================

This is a list of all tested Samsung hard drive models and their ratings. See more
info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Samsung   | SV1021H            | 10 GB  | 1       | 5713  | 0     | 15.65  |
| Samsung   | HE160HJ            | 160 GB | 1       | 2967  | 0     | 8.13   |
| Samsung   | HE103SJ            | 1 TB   | 1       | 1865  | 0     | 5.11   |
| Samsung   | SP0451N            | 40 GB  | 1       | 1237  | 0     | 3.39   |
| Samsung   | HD400LD            | 400 GB | 2       | 1157  | 0     | 3.17   |
| Samsung   | HD160JJ-P          | 160 GB | 7       | 1513  | 436   | 2.84   |
| Samsung   | HD040GJ            | 40 GB  | 3       | 1360  | 41    | 2.82   |
| Samsung   | HD040GJ-P          | 40 GB  | 1       | 975   | 0     | 2.67   |
| Samsung   | HD300LJ            | 304 GB | 7       | 1073  | 487   | 1.95   |
| Samsung   | HD103UJ            | 1 TB   | 35      | 938   | 167   | 1.71   |
| Samsung   | HD083GJ            | 80 GB  | 2       | 613   | 0     | 1.68   |
| Samsung   | HD103SI            | 1 TB   | 23      | 906   | 142   | 1.64   |
| Samsung   | HD320KJ            | 320 GB | 2       | 595   | 0     | 1.63   |
| Samsung   | SP1644N            | 160 GB | 3       | 683   | 527   | 1.58   |
| Samsung   | HD103SJ            | 1 TB   | 73      | 736   | 13    | 1.57   |
| Samsung   | HD253GJ            | 250 GB | 7       | 768   | 17    | 1.53   |
| Samsung   | HD204UI            | 2 TB   | 17      | 871   | 57    | 1.52   |
| Samsung   | HD321HJ            | 320 GB | 4       | 909   | 39    | 1.46   |
| Samsung   | HD642JJ            | 640 GB | 17      | 1048  | 320   | 1.39   |
| Samsung   | HD502HJ            | 500 GB | 79      | 697   | 85    | 1.36   |
| Samsung   | HD502IJ            | 500 GB | 25      | 918   | 299   | 1.35   |
| Samsung   | HD252HJ            | 250 GB | 19      | 794   | 135   | 1.29   |
| Samsung   | SV0221N            | 20 GB  | 1       | 2328  | 4     | 1.28   |
| Samsung   | HD105SI            | 1 TB   | 7       | 686   | 552   | 1.26   |
| Samsung   | HD153WI            | 1.5 TB | 1       | 451   | 0     | 1.24   |
| Samsung   | HD501LJ            | 500 GB | 33      | 808   | 344   | 1.20   |
| Samsung   | HD155UI            | 1.5 TB | 1       | 437   | 0     | 1.20   |
| Samsung   | HD161GJ            | 160 GB | 16      | 654   | 134   | 1.15   |
| Samsung   | SP1654N            | 160 GB | 7       | 841   | 321   | 1.14   |
| Samsung   | HM251HI            | 250 GB | 4       | 532   | 3     | 1.11   |
| Samsung   | HD753LJ            | 752 GB | 24      | 927   | 249   | 1.09   |
| Samsung   | HD161HJ 41R0186LEN | 160 GB | 1       | 380   | 0     | 1.04   |
| Samsung   | HD154UI            | 1.5 TB | 27      | 813   | 282   | 1.02   |
| Samsung   | SP1613N            | 160 GB | 1       | 1079  | 2     | 0.99   |
| Samsung   | HD251HJ            | 250 GB | 7       | 753   | 62    | 0.94   |
| Samsung   | HD080HJ            | 80 GB  | 83      | 781   | 351   | 0.93   |
| Samsung   | HD322HJ            | 320 GB | 20      | 627   | 92    | 0.92   |
| Samsung   | HD403LJ            | 400 GB | 14      | 1119  | 516   | 0.89   |
| Samsung   | HD752LJ            | 752 GB | 2       | 1517  | 4     | 0.87   |
| Samsung   | SP0812N            | 80 GB  | 6       | 607   | 4     | 0.85   |
| Samsung   | HD400LJ            | 400 GB | 2       | 480   | 5     | 0.84   |
| Samsung   | HD503HI            | 500 GB | 14      | 555   | 18    | 0.83   |
| Samsung   | HD502HI            | 500 GB | 17      | 526   | 98    | 0.83   |
| Samsung   | HM100UI            | 1 TB   | 2       | 303   | 0     | 0.83   |
| Samsung   | HD321KJ            | 320 GB | 51      | 740   | 292   | 0.83   |
| Samsung   | HD103UI            | 1 TB   | 2       | 483   | 9     | 0.83   |
| Samsung   | HM120JC            | 120 GB | 2       | 600   | 51    | 0.82   |
| Samsung   | HM641JI            | 640 GB | 17      | 484   | 63    | 0.82   |
| Samsung   | SV1203N            | 120 GB | 2       | 441   | 3     | 0.80   |
| Samsung   | HM321HI            | 320 GB | 70      | 452   | 91    | 0.79   |
| Samsung   | HN-M500MBB         | 500 GB | 27      | 410   | 4     | 0.77   |
| Samsung   | HD160JJ            | 160 GB | 59      | 839   | 457   | 0.71   |
| Samsung   | HM500JI            | 500 GB | 19      | 450   | 4     | 0.70   |
| Samsung   | HD250HJ            | 250 GB | 30      | 778   | 649   | 0.69   |
| Samsung   | HD163GJ            | 160 GB | 1       | 251   | 0     | 0.69   |
| Samsung   | HM320HJ            | 320 GB | 3       | 419   | 422   | 0.67   |
| Samsung   | SP0411N            | 40 GB  | 1       | 4382  | 17    | 0.67   |
| Samsung   | HM320II            | 320 GB | 15      | 403   | 272   | 0.66   |
| Samsung   | MP0402H            | 40 GB  | 3       | 284   | 2     | 0.66   |
| Samsung   | HD322GJ            | 320 GB | 18      | 423   | 68    | 0.66   |
| Samsung   | HM250JI            | 250 GB | 5       | 537   | 9     | 0.64   |
| Samsung   | HD120IJ            | 120 GB | 18      | 784   | 272   | 0.62   |
| Samsung   | SP1624N            | 160 GB | 1       | 225   | 0     | 0.62   |
| Samsung   | HM250HI            | 250 GB | 59      | 320   | 16    | 0.59   |
| Samsung   | SP0812C            | 80 GB  | 13      | 513   | 161   | 0.57   |
| Samsung   | HN-M320MBB         | 320 GB | 3       | 401   | 3     | 0.57   |
| Samsung   | SP0802N            | 80 GB  | 24      | 691   | 52    | 0.55   |
| Samsung   | HD161HJ            | 160 GB | 36      | 898   | 546   | 0.53   |
| Samsung   | HM321HX            | 320 GB | 2       | 263   | 4     | 0.50   |
| Samsung   | HE753LJ            | 752 GB | 2       | 374   | 51    | 0.49   |
| Samsung   | HM080HC            | 72 GB  | 1       | 526   | 2     | 0.48   |
| Samsung   | HM501II            | 500 GB | 3       | 596   | 5     | 0.47   |
| Samsung   | HM250HJ            | 250 GB | 2       | 337   | 17    | 0.46   |
| Samsung   | HD080HJ-P          | 80 GB  | 8       | 572   | 266   | 0.46   |
| Samsung   | HD082GJ            | 80 GB  | 11      | 499   | 402   | 0.44   |
| Samsung   | HD401LJ            | 400 GB | 3       | 712   | 19    | 0.41   |
| Samsung   | SP1634N            | 160 GB | 1       | 298   | 1     | 0.41   |
| Samsung   | SP0842N            | 80 GB  | 8       | 634   | 591   | 0.40   |
| Samsung   | SP2504C            | 250 GB | 30      | 1070  | 826   | 0.39   |
| Samsung   | SP0822N            | 80 GB  | 6       | 130   | 1     | 0.35   |
| Samsung   | HM500JJ            | 500 GB | 2       | 331   | 5     | 0.35   |
| Samsung   | SP2004C            | 200 GB | 26      | 668   | 489   | 0.35   |
| Samsung   | HN-M101MBB         | 1 TB   | 9       | 450   | 371   | 0.28   |
| Samsung   | SP1604N            | 160 GB | 3       | 394   | 62    | 0.28   |
| Samsung   | HD252KJ            | 250 GB | 7       | 730   | 436   | 0.27   |
| Samsung   | SP0401N            | 40 GB  | 1       | 95    | 0     | 0.26   |
| Samsung   | HD200HJ            | 200 GB | 14      | 867   | 739   | 0.26   |
| Samsung   | HS082HB            | 80 GB  | 1       | 89    | 0     | 0.25   |
| Samsung   | HN-M750MBB         | 752 GB | 4       | 229   | 7     | 0.24   |
| Samsung   | HD300LD            | 304 GB | 4       | 554   | 15    | 0.23   |
| Samsung   | SP2514N            | 250 GB | 6       | 419   | 509   | 0.20   |
| Samsung   | SP2014N            | 200 GB | 5       | 362   | 219   | 0.18   |
| Samsung   | HM120JI            | 120 GB | 4       | 327   | 5     | 0.16   |
| Samsung   | SP1213N            | 120 GB | 3       | 1017  | 346   | 0.15   |
| Samsung   | SP1213C            | 120 GB | 5       | 677   | 45    | 0.14   |
| Samsung   | SP1203N            | 120 GB | 6       | 486   | 31    | 0.13   |
| Samsung   | SV4012H            | 40 GB  | 2       | 1504  | 236   | 0.12   |
| Samsung   | HM060HI            | 64 GB  | 1       | 541   | 13    | 0.11   |
| Samsung   | HM100JC            | 100 GB | 1       | 383   | 9     | 0.10   |
| Samsung   | HM320JI            | 320 GB | 4       | 419   | 10    | 0.10   |
| Samsung   | SV0401N            | 40 GB  | 1       | 952   | 31    | 0.08   |
| Samsung   | MP0804H            | 80 GB  | 3       | 243   | 8     | 0.08   |
| Samsung   | SV0412H            | 40 GB  | 4       | 957   | 88    | 0.08   |
| Samsung   | HM160HC            | 160 GB | 6       | 187   | 23    | 0.07   |
| Samsung   | HD160HJ            | 160 GB | 13      | 552   | 732   | 0.06   |
| Samsung   | HM251JI            | 250 GB | 6       | 334   | 527   | 0.06   |
| Samsung   | HM160HI            | 160 GB | 46      | 364   | 345   | 0.06   |
| Samsung   | HM080HI            | 80 GB  | 2       | 473   | 405   | 0.05   |
| Samsung   | HM121HC            | 120 GB | 2       | 179   | 26    | 0.05   |
| Samsung   | HN-M250MBB         | 250 GB | 1       | 292   | 17    | 0.04   |
| Samsung   | SP1253N            | 120 GB | 1       | 286   | 18    | 0.04   |
| Samsung   | SP1614C            | 160 GB | 2       | 124   | 22    | 0.04   |
| Samsung   | HM502JX            | 500 GB | 1       | 11    | 0     | 0.03   |
| Samsung   | SP1614N            | 160 GB | 1       | 690   | 106   | 0.02   |
| Samsung   | HM121HI            | 120 GB | 8       | 460   | 1255  | 0.02   |
| Samsung   | HM100JI            | 100 GB | 1       | 827   | 140   | 0.02   |
| Samsung   | HM500LI            | 500 GB | 1       | 7     | 1     | 0.01   |
| Samsung   | HM160JI            | 160 GB | 3       | 605   | 748   | 0.01   |
| Samsung   | SV0802N            | 80 GB  | 1       | 554   | 215   | 0.01   |
| Samsung   | HD251KJ            | 250 GB | 1       | 1539  | 1019  | 0.00   |
| Samsung   | SV2011H            | 20 GB  | 1       | 0     | 3     | 0.00   |
| Samsung   | HS06THB            | 64 GB  | 1       | 2     | 2087  | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Samsung   | SpinPoint V20400       | 1      | 1       | 5713  | 0     | 15.65  |
| Samsung   | SpinPoint F3 RE        | 1      | 1       | 1865  | 0     | 5.11   |
| Samsung   | SpinPoint              | 2      | 3       | 1164  | 3     | 3.04   |
| Samsung   | SpinPoint F4 EG (AF)   | 2      | 18      | 847   | 54    | 1.50   |
| Samsung   | SpinPoint F3           | 4      | 160     | 715   | 49    | 1.46   |
| Samsung   | SpinPoint T133         | 5      | 18      | 841   | 197   | 1.32   |
| Samsung   | SpinPoint F1 DT        | 11     | 171     | 860   | 186   | 1.30   |
| Samsung   | SpinPoint VL40         | 1      | 1       | 2328  | 4     | 1.28   |
| Samsung   | SpinPoint F2 EG        | 3      | 67      | 772   | 187   | 1.18   |
| Samsung   | SpinPoint F3 EG        | 3      | 22      | 592   | 187   | 0.99   |
| Samsung   | SpinPoint P80 SD       | 7      | 179     | 831   | 370   | 0.92   |
| Samsung   | SpinPoint MT2          | 1      | 2       | 303   | 0     | 0.83   |
| Samsung   | SpinPoint T166         | 7      | 121     | 786   | 389   | 0.83   |
| Samsung   | SpinPoint F1 EG        | 1      | 2       | 483   | 9     | 0.83   |
| Samsung   | SpinPoint M7E (AF)     | 4      | 94      | 466   | 79    | 0.80   |
| Samsung   | SpinPoint PL40         | 1      | 1       | 4382  | 17    | 0.67   |
| Samsung   | SpinPoint F4           | 1      | 18      | 423   | 68    | 0.66   |
| Samsung   | SpinPoint M7           | 3      | 93      | 360   | 55    | 0.63   |
| Samsung   | SpinPoint M8 (AF)      | 5      | 44      | 398   | 80    | 0.59   |
| Samsung   | SpinPoint P80          | 18     | 92      | 590   | 149   | 0.56   |
| Samsung   | SpinPoint S250         | 2      | 44      | 806   | 678   | 0.55   |
| Samsung   | SpinPoint MP5          | 3      | 7       | 371   | 187   | 0.52   |
| Samsung   | SpinPoint S166         | 3      | 48      | 796   | 502   | 0.52   |
| Samsung   | SpinPoint F1 RE        | 1      | 2       | 374   | 51    | 0.49   |
| Samsung   | SpinPoint V80          | 3      | 4       | 597   | 63    | 0.42   |
| Samsung   | SpinPoint M            | 2      | 6       | 263   | 5     | 0.37   |
| Samsung   | SpinPoint P120         | 5      | 68      | 807   | 612   | 0.35   |
| Samsung   | SpinPoint M40/60/80    | 8      | 15      | 503   | 223   | 0.21   |
| Samsung   | SpinPoint N2           | 2      | 2       | 46    | 1044  | 0.12   |
| Samsung   | SpinPoint M5           | 5      | 67      | 367   | 390   | 0.10   |
| Samsung   | SpinPoint V40+         | 2      | 3       | 1003  | 159   | 0.08   |
| Samsung   | SpinPoint V60          | 1      | 4       | 957   | 88    | 0.08   |
| Samsung   | SpinPoint M6           | 3      | 11      | 335   | 291   | 0.07   |
| Samsung   | SpinPoint M7U (USB)    | 1      | 1       | 11    | 0     | 0.03   |
